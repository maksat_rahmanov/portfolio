export class Constants {
  static readonly themeCookie: string = 'pfl-theme';
  static readonly themeDark: string = 'dark';
  static readonly themeLight: string = 'light';
}
