import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ProjectsService } from '@shared/services/projects.facade';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeComponent implements OnInit, OnDestroy {
  items$!: Observable<Project[]>;

  private destroy$ = new Subject<boolean>();

  constructor(
    private projectService: ProjectsService,
    private route: ActivatedRoute,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    this.items$ = this.projectService.getFilteredItems();
    this.titleService.setTitle('Portfolio');

    this.route.queryParams.pipe(takeUntil(this.destroy$)).subscribe((param) => {
      this.projectService.setTag(param?.tag);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
