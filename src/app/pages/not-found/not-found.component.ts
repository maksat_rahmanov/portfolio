import { Component } from '@angular/core';

@Component({
  selector: 'not-found',
  template: `
    <product-hero
      title="Page not found"
      description="Please check if page exists and url is correct"
    ></product-hero>
    <div class="nothing wrapper">
      <pf-image
        width="475"
        height="300"
        srcSmall="assets/images/nothing-found-sm.png"
        src="assets/images/nothing-found.png"
        alt="nothing found"
      ></pf-image>
    </div>
  `,
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {}
