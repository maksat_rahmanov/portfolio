import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { DetailRoutingModule } from './detail-routing.module';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [ProjectDetailComponent],
  imports: [CommonModule, DetailRoutingModule, SharedModule, NgOptimizedImage]
})
export class DetailModule {}
