import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { projectDetailResolver } from './project-detail.resolver';

const routes: Routes = [
  {
    path: ':id',
    component: ProjectDetailComponent,
    resolve: {
      data: projectDetailResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailRoutingModule {}
