import { inject } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  ResolveFn,
  RouterStateSnapshot
} from '@angular/router';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { ProjectApiService } from '@graphcms/api/project.api';

export const projectDetailResolver: ResolveFn<ProjectDetail | undefined> = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
): Observable<ProjectDetail | undefined> => {
  const projectService = inject(ProjectApiService);
  const id = route.paramMap.get('id') || '';

  return projectService.getProject(id).pipe(first());
};
