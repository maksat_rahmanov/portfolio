import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProjectDetailComponent implements OnInit, OnDestroy {
  details!: ProjectDetail;
  projectResource!: ProjectResource;
  private destroy$ = new Subject<boolean>();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private titleService: Title
  ) {}

  ngOnInit(): void {
    this.route.data.pipe(takeUntil(this.destroy$)).subscribe((projectData) => {
      const project = projectData['data'];
      if (!project) {
        this.router.navigate(['not-found']);
        return;
      }

      this.details = project;
      this.projectResource = {
        layoutUrl: this.details.layoutUrl,
        liveUrl: this.details.liveUrl,
        sourceCodeLink: this.details.sourceCodeLink,
        downloadLink: this.details.downloadLink
      };
      this.titleService.setTitle(`Portfolio | ${this.details.title}`);
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }
}
