import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { ContactRoutingModule } from './contact-routing.module';
import { ContactInfoComponent } from './contact-info/contact-info.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [ContactInfoComponent],
  imports: [CommonModule, ContactRoutingModule, SharedModule, NgOptimizedImage]
})
export class ContactModule {}
