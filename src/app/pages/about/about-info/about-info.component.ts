import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-about-info',
  templateUrl: './about-info.component.html',
  styleUrls: ['./about-info.component.scss']
})
export class AboutInfoComponent implements OnInit {
  projectResource: ProjectResource;

  constructor(private titleService: Title) {
    this.projectResource = {
      sourceCodeLink:
        'https://bitbucket.org/maksat_rahmanov/portfolio/src/master/',
      layoutUrl:
        'https://www.figma.com/file/MmoXWGGPp9k7ZoUgKw62oS/Portfolio?type=design&node-id=0%3A1&mode=design&t=uXZv0NwkK0PZo1BM-1'
    };
  }

  ngOnInit(): void {
    this.titleService.setTitle('Portfolio | About');
  }
}
