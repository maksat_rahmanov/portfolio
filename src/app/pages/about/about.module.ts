import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { AboutRoutingModule } from './about-routing.module';
import { AboutInfoComponent } from './about-info/about-info.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [AboutInfoComponent],
  imports: [CommonModule, AboutRoutingModule, SharedModule, NgOptimizedImage]
})
export class AboutModule {}
