import { Injectable, Injector } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { ProjectApiService } from '@graphcms/api/project.api';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  private projects = new BehaviorSubject<Project[]>([]);
  private filteredItems = new BehaviorSubject<Project[]>([]);
  private selectedTag = new BehaviorSubject<string>('');

  constructor(private injector: Injector) {}

  init(): Promise<Project[]> {
    //return new Promise<Project[]>((resolve) => resolve([]));
    return new Promise((resolve) => {
      this.injector
        .get(ProjectApiService)
        .getProjects()
        .pipe(
          catchError((err) => {
            this.projects.next([]);
            resolve(err);
            return throwError('Could not load portfolio projects');
          })
        )
        .subscribe((response: Project[]) => {
          this.projects.next(response);
          this.selectedTag.subscribe((tag) => {
            this.filterItemsByTag(tag);
          });
          resolve(response);
        });
    });
  }

  setTag(tag: string): void {
    this.selectedTag.next(tag);
  }

  getFilteredItems(): Observable<Project[]> {
    return this.filteredItems;
  }

  private filterItemsByTag(tag: string): void {
    if (!tag) {
      this.filteredItems.next(this.projects.getValue());
      return;
    }

    const filtered = this.projects
      .getValue()
      .filter((item) => item.tags.includes(tag));
    this.filteredItems.next(filtered);
  }
}
