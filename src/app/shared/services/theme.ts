import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { Constants } from '../../app.constants';
import { CookieService } from 'ngx-cookie';
import { Meta } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private readonly expirationDays = 30;
  private date = new Date();
  private themeColor: ReadonlyMap<string, string> = new Map([
    [Constants.themeLight, '#ffffff'],
    [Constants.themeDark, '#1a1a1a']
  ]);

  private themeSubject = new BehaviorSubject<string>('');

  constructor(
    @Inject(DOCUMENT) private document: Document,
    private meta: Meta,
    private cookieService: CookieService
  ) {
    const expDaysInMs = this.expirationDays * 24 * 3600 * 1000;
    this.date.setTime(this.date.getTime() + expDaysInMs);
  }

  getTheme(): Observable<string> {
    return this.themeSubject.asObservable();
  }

  setTheme(theme: string): void {
    const appTheme = theme.toLowerCase();

    this.cookieService.put(Constants.themeCookie, appTheme);
    this.themeSubject.next(appTheme);

    this.document.body.classList.value = appTheme;
    const color = this.themeColor.get(appTheme);

    if (color) {
      this.meta.updateTag({ name: 'theme-color', content: color });
    }
  }
}
