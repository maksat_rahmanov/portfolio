import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { ProductHeroComponent } from './components/product-hero/product-hero.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { CookieModule } from 'ngx-cookie';
import { ActionButtonsComponent } from './components/action-buttons/action-buttons.component';
import { ProgressiveImageComponent } from './components/image/image.component';

@NgModule({
  declarations: [
    ProductHeroComponent,
    ProjectsComponent,
    HeaderComponent,
    ProgressiveImageComponent,
    ActionButtonsComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    NgOptimizedImage,
    CookieModule.withOptions()
  ],
  exports: [
    ProductHeroComponent,
    ProjectsComponent,
    ProgressiveImageComponent,
    HeaderComponent,
    ActionButtonsComponent
  ]
})
export class SharedModule {}
