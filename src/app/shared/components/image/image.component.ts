import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'pf-image',
  template: `<img
    class="img"
    [src]="imgPath"
    [width]="width"
    [height]="height"
    [alt]="alt"
    [class.loaded]="loaded"
  />`,
  styleUrls: ['./image.component.scss']
})
export class ProgressiveImageComponent implements OnInit {
  @Input() width!: string;
  @Input() height!: string;
  @Input() alt?: string;
  @Input() srcSmall!: string;
  @Input() src!: string;

  loaded = false;
  private path = '';

  ngOnInit(): void {
    this.path = this.srcSmall;
    const image = new Image();

    image.src = this.src;
    image.onload = () => {
      this.path = this.src;
      this.loaded = true;
    };
  }

  get imgPath() {
    return this.path;
  }
}
