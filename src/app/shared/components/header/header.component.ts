import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild
} from '@angular/core';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';
import { ThemeService } from '../../services/theme';
import { Constants } from '../../../app.constants';

interface MenuItem {
  tag: string;
  name: string;
}

@Component({
  selector: 'portfolio-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent implements OnInit, OnDestroy {
  @ViewChild('navigation') navigation!: ElementRef;
  @ViewChild('menuToggle') menuToggle!: ElementRef;

  isDarkTheme = false;

  menuItems: MenuItem[] = [
    {
      tag: 'angular',
      name: 'Angular'
    },
    {
      tag: 'react',
      name: 'React'
    },
    {
      tag: 'website',
      name: 'Websites'
    }
  ];
  isMenuOpen = false;

  private readonly menuOpen = 'menu-open';
  private destroy$ = new Subject<boolean>();

  constructor(private router: Router, private themeService: ThemeService) {}

  ngOnInit(): void {
    this.themeService
      .getTheme()
      .pipe(takeUntil(this.destroy$))
      .subscribe((theme) => {
        this.isDarkTheme = theme === Constants.themeDark;
      });

    this.router.events
      .pipe(
        takeUntil(this.destroy$),
        filter((event) => event instanceof NavigationEnd)
      )
      .subscribe(() => {
        this.navigation.nativeElement.classList.remove(this.menuOpen);
        this.menuToggle.nativeElement.classList.remove(this.menuOpen);
        this.isMenuOpen = false;
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

  onMenuToggle(): void {
    this.navigation.nativeElement.classList.toggle(this.menuOpen);
    this.menuToggle.nativeElement.classList.toggle(this.menuOpen);
    this.isMenuOpen = !this.isMenuOpen;
  }

  switchTheme(): void {
    this.themeService.setTheme(
      this.isDarkTheme ? Constants.themeLight : Constants.themeDark
    );
  }
}
