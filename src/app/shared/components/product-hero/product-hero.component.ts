import { animate, style, transition, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  Component,
  HostBinding,
  Input
} from '@angular/core';

@Component({
  selector: 'product-hero',
  template:
    '<h1 [style.color]="color">{{ title }}</h1><p [style.color]="color">{{ description }}</p>',
  styleUrls: ['./product-hero.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('scrollTop', [
      transition('* => *', [
        style({
          opacity: 0,
          transform: 'translateY(30%)'
        }),
        animate(800)
      ])
    ])
  ]
})
export class ProductHeroComponent {
  @HostBinding('@scrollTop')
  @Input()
  title = '';
  @Input() description = '';
  @Input() color?: string;
}
