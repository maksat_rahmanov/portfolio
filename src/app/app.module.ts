import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { SharedModule } from '@shared/shared.module';
import { ProjectsService } from '@shared/services/projects.facade';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { GraphCmsModule } from '@graphcms/graphcms.module';
import { HttpClientModule } from '@angular/common/http';
import { ThemeService } from '@shared/services/theme';
import { Constants } from './app.constants';
import { CookieModule, CookieService } from 'ngx-cookie';
import { NgOptimizedImage } from '@angular/common';

@NgModule({
  declarations: [AppComponent, HomeComponent, NotFoundComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    GraphCmsModule,
    CookieModule.withOptions(),
    NgOptimizedImage
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initApp,
      deps: [ProjectsService, ThemeService, CookieService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

function initApp(
  projectsService: ProjectsService,
  themeService: ThemeService,
  cookieService: CookieService
) {
  return () =>
    new Promise<void>((resolve, reject) => {
      const theme = cookieService.get(Constants.themeCookie) || '';
      themeService.setTheme(theme);

      projectsService
        .init()
        .then(() => resolve())
        .catch(() => reject());
    });
}
