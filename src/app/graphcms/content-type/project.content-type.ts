interface Asset {
  url: string;
}

interface RichText {
  html: string;
}

interface Tag {
  title: string;
}

interface ProjectContentTypeBase {
  id: string;
  title: string;
}

export interface ProjectContentType extends ProjectContentTypeBase {
  previewImage: Asset;
  tags: Tag[];
}

export interface ProjectDetailContentType extends ProjectContentTypeBase {
  shortDescription: string;
  description: string;
  liveUrl?: string;
  layoutUrl?: string;
  sourceCodeLink: string;
  downloadLink?: string;
  detailImage: Asset;
  features: RichText;
  devStack: RichText;
}
