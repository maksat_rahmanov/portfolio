import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Apollo } from 'apollo-angular';
import { getProjectsQuery } from '../query/projects.query';
import { map } from 'rxjs/operators';
import {
  ProjectContentType,
  ProjectDetailContentType
} from '../content-type/project.content-type';
import { getProjectQuery } from '../query/project.query';

interface ProjectsQuery {
  projects: ProjectContentType[];
}

interface ProjectQuery {
  project: ProjectDetailContentType;
}

@Injectable({
  providedIn: 'root'
})
export class ProjectApiService {
  constructor(private apollo: Apollo) {}

  getProjects(): Observable<Project[]> {
    return this.apollo
      .query<ProjectsQuery>({
        query: getProjectsQuery
      })
      .pipe(map((response) => response.data.projects.map(this.mapToProject)));
  }

  getProject(id: string): Observable<ProjectDetail> {
    return this.apollo
      .query<ProjectQuery>({
        query: getProjectQuery,
        variables: { id }
      })
      .pipe(map((response) => this.mapToDetailProject(response.data.project)));
  }

  private mapToProject(project: ProjectContentType): Project {
    return {
      id: project.id,
      title: project.title,
      previewImage: project.previewImage.url,
      tags: project.tags.map((tag) => tag.title)
    };
  }

  private mapToDetailProject(project: ProjectDetailContentType): ProjectDetail {
    return {
      id: project.id,
      title: project.title,
      detailImage: project.detailImage.url,
      sourceCodeLink: project.sourceCodeLink,
      downloadLink: project.downloadLink,
      liveUrl: project.liveUrl,
      layoutUrl: project.layoutUrl,
      features: project.features.html,
      devStack: project.devStack.html,
      shortDescription: project.shortDescription,
      description: project.description
    };
  }
}
