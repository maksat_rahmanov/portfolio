import { gql } from 'apollo-angular';

export const getProjectsQuery = gql`
  query {
    projects {
      id
      title
      previewImage {
        url
      }
      tags {
        title
      }
    }
  }
`;
