import { gql } from 'apollo-angular';

export const getProjectQuery = gql`
  query ($id: ID) {
    project(where: { id: $id }) {
      title
      shortDescription
      description
      liveUrl
      layoutUrl
      sourceCodeLink
      downloadLink
      detailImage {
        url
      }
      features {
        html
      }
      devStack {
        html
      }
    }
  }
`;
