interface ProjectBase {
  id: string;
  title: string;
}

interface Project extends ProjectBase {
  previewImage: string;
  tags: string[];
}

interface ProjectDetail extends ProjectBase {
  shortDescription: string;
  description: string;
  liveUrl?: string;
  layoutUrl?: string;
  sourceCodeLink?: string;
  downloadLink?: string;
  detailImage: string;
  features: string;
  devStack: string;
}

type ProjectResource = Pick<
  ProjectDetail,
  'liveUrl' | 'layoutUrl' | 'sourceCodeLink' | 'downloadLink'
>;
