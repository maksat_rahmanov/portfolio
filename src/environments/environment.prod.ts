export const environment = {
  production: true,
  graphCmsEndpoint:
    'https://eu-central-1.cdn.hygraph.com/content/ckxv0vpck0bxd01xna32j1xa4/master'
};
