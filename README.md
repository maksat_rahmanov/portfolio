# Portfolio

Personal portfolio website.

UI design inspired by [Sassio](https://preview.themeforest.net/item/sassio-software-saas-html5-template/full_screen_preview/35080519).

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12 with update to version 15.

## Live preview
[https://portfolio-sln.vercel.app](https://portfolio-sln.vercel.app)

## Figma layout
[Project layout](https://www.figma.com/file/MmoXWGGPp9k7ZoUgKw62oS/Portfolio?type=design&mode=design&t=dcqsAWXbCCpgjtgS-1)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

[//]: # (## Dev SSR version)

[//]: # ()
[//]: # (Run `dev:ssr` for a dev SSR application.)

[//]: # ()
[//]: # (## Serve built SSR version)

[//]: # (Build application with `build:ssr` command then run `serve:ssr`. Navigate to `http://localhost:4000/`.)
